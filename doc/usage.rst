Using the tools
===============


Using the command-line scripts
------------------------------

Two use the tools two ini files must be made:

#. osm2hydro.ini - To define the steps to take, define width of lines elements, geographical regions etc.
#. osm2shp.ini - Define how to include OSM tags in shape files. See the osm2shp help fro details

Once these are made you can run osm2hydro:

osm2hydro -c osm2hyro.ini

See the example directory fror further information. The easiest way to start is to take an 
example and adjust it to your needs.


Using the tools from within python
----------------------------------

See the ipython notebooks for examples


